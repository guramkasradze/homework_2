﻿using System;
using System.Collections.Generic;

namespace HomeWork_2
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleUI.Weapons.Add((Weapon)GameManager.CreateWeapon("MK14", 61));
            ConsoleUI.Weapons.Add((Weapon)GameManager.CreateWeapon("SLR", 58));
            ConsoleUI.Weapons.Add((Weapon)GameManager.CreateWeapon("Mini 14", 46));
            ConsoleUI.Weapons.Add((Weapon)GameManager.CreateWeapon("SKS", 53));
            ConsoleUI.Weapons.Add((Weapon)GameManager.CreateWeapon("VSS", 41));

            ConsoleUI.GetPlayers();
            ConsoleUI.DeclareWinner();
        }
    }
}
