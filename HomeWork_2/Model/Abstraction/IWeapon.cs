﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_2
{
    public interface IWeapon
    {
        int WeaponId { get; set; }
        string Name { get; set; }
        double Damage { get; set; }
    }
}
