﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_2.Model.Abstraction
{
    public interface IPlayer
    {
        int PlayerId { get; set; }
        string Name { get; set; }
        IWeapon MyWeapon { get; set; }
    }
}
    