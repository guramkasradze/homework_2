﻿using HomeWork_2.Model.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_2
{
    public class Player : IPlayer
    {
        public int PlayerId { get; set; }
        public string Name { get; set; }
        public IWeapon MyWeapon { get; set; }

        public Player(int playerId, string name, IWeapon weapon)
        {
            PlayerId = playerId;
            Name = name;
            MyWeapon = weapon;
        }
    }
}
