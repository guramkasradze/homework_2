﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_2
{
    public class Weapon : IWeapon
    {
        public int WeaponId { get; set; }
        public string Name { get; set; }
        public double Damage { get; set; }

        public Weapon(int weaponId, string name, double damage)
        {
            WeaponId = weaponId;
            Name = name;
            Damage = damage;
        }
    }
}
