﻿using HomeWork_2.Model.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HomeWork_2
{
    public static class GameManager
    {
        private static int PlayerId = 0;
        private static int WeaponId = 0;

        public static IWeapon CreateWeapon(string name, double damage)
        {
            return new Weapon(++WeaponId, name, damage);
        }

        public static IPlayer CreatePlayer(string name, IWeapon weapon)
        {
            return new Player(++PlayerId, name, weapon);
        }

        public static IEnumerable<IPlayer> GetWinner(IEnumerable<IPlayer> players)
        {
            List<IPlayer> winners = new List<IPlayer>();
            
            foreach (var player in players)
            {
                if (winners.Count == 0)
                {
                    winners.Add(player);
                }
                else if (player.MyWeapon.Damage > winners[0].MyWeapon.Damage)
                {
                    winners.Clear();
                    winners.Add(player);
                }
                else if (player.MyWeapon.Damage == winners[0].MyWeapon.Damage)
                {
                    winners.Add(player);
                }
            }

            return winners;
        }
    }
}
