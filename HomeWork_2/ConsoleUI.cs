﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HomeWork_2
{
    public static class ConsoleUI
    {
        public static List<Player> Players { get; set; } = new List<Player>();
        public static List<Weapon> Weapons { get; set; } = new List<Weapon>();

        private static int GetPlayerCount()
        {
            try
            {
                Console.Write("Input Player Count: ");
                var pc = Convert.ToInt32(Console.ReadLine());

                return pc;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message}. Please enter valid type...");
            }

            return GetPlayerCount();
        }

        public static void GetPlayers()
        {
            var count = GetPlayerCount();

            for (int i = 0; i < count; i++)
            {
                Console.Write($"\nPlayer{i + 1} name: ");
                var name = Console.ReadLine();
                var weapon = GetWeapon();

                Players.Add((Player)GameManager.CreatePlayer(name, weapon));
            }
        }

        public static IWeapon GetWeapon()
        {
            Console.WriteLine();
            foreach (var weapon in Weapons)
            {
                Console.WriteLine($"{weapon.WeaponId}) {weapon.Name}");
            }

            try
            {
                Console.Write("\nChoose weapon by id: ");
                var weaponId = Convert.ToInt32(Console.ReadLine());
                var chosenwp = Weapons.First(i => i.WeaponId == weaponId);

                return chosenwp;
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message}. Please enter valid type...");
            }

            return GetWeapon();
        }

        public static void DeclareWinner()
        {
            Console.WriteLine("\n\nWinner(s) of current round:");
            var winners = GameManager.GetWinner(Players);

            foreach (var winner in winners)
            {
                Console.WriteLine($"{winner.PlayerId}) {winner.Name} {winner.MyWeapon.Name} {winner.MyWeapon.Damage}");
            }
        }
    }
}
